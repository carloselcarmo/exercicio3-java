package br.com.Itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class IO
{
    public static Map<String, Integer> solicitarInicioAgenda()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.printf("Informe a Quantidade de Contatos que você deseja cadastrar: ");
        int qtdPessoas = scanner.nextInt();

        Map<String, Integer> dados = new HashMap<>();
        dados.put("qtdPessoas", qtdPessoas);

        return dados;
    }

    public static Map<String, String> solicitarDadosContato()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Informe o Nome do Contato:");
        String nome = scanner.nextLine();

        System.out.println("Informe o Telefone do Contato");
        String telefone = scanner.nextLine();

        System.out.println("Informe o E-mail do Contato");
        String email = scanner.nextLine();

        Map<String, String> dados = new HashMap<>();
        dados.put("nome", nome);
        dados.put("telefone", telefone);
        dados.put("email", email);

        return dados;
    }

    public static void imprimir(Impressao impressao)
    {
        impressao.imprimir();
    }

    public static void imprimirException(ArithmeticException ex)
    {
        System.out.println(ex.getMessage());
    }

    public static Map<String, String> solicitarTextoParaBuscaImpressao()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.printf("Informe o e-mail ou telefone do contato que você deseja consultar: ");
        String texto = scanner.nextLine();

        Map<String, String> dados = new HashMap<>();
        dados.put("texto", texto);

        return dados;
    }

    public static Map<String, String> solicitarTextoParaBuscaRemover()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.printf("Informe o e-mail do contato que você deseja remover: ");
        String texto = scanner.nextLine();

        Map<String, String> dados = new HashMap<>();
        dados.put("texto", texto);

        return dados;
    }

    public static void exibirMensagemExclusao()
    {
        System.out.println("Contato removido com sucesso");
    }

    public static void exibirMensagemInclusao()
    {
        System.out.println("Contato adicionado com sucesso");
    }

    public static Map<String, Integer> solicitarOpcaoMenu()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.printf("Informe a opção da agenda que você deseja acessar: ");
        System.out.printf("(1) Imprimir Contato");
        System.out.printf("(2) Remover Contato");
        System.out.printf("(3) Adicionar Contato");
        System.out.printf("(4) Imprimir Agenda");
        System.out.printf("(5) Sair ");
        Integer opcao = scanner.nextInt();

        Map<String, Integer> dados = new HashMap<>();
        dados.put("opcao", opcao);

        return dados;
    }
}
