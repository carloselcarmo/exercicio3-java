package br.com.Itau;

public class Contato extends Pessoa implements Impressao
{
    private String telefone;
    private String email;

    public String getTelefone()
    {
        return telefone;
    }

    public void setTelefone(String telefone)
    {
        telefone = telefone;
    }

    public String getEmail()
    {
        return email;
    }

    public Contato(String nome, String telefone, String email)
    {
        super(nome);
        this.telefone = telefone;
        this.email = email;
    }

    @Override
    public void imprimir()
    {
        System.out.println("Nome: " + super.getNome());
        System.out.println("Telefone: " + getTelefone());
        System.out.println("E-mail: " + getEmail());
    }
}
