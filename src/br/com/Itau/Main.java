package br.com.Itau;

import javax.management.modelmbean.RequiredModelMBean;
import java.util.Map;

public class Main {

    public static void main(String[] args)
    {
        Map<String, Integer> dadosQtdPessoas  = IO.solicitarInicioAgenda();

        Agenda  agenda = new Agenda(dadosQtdPessoas.get("qtdPessoas"));

        Integer opcaoMenu = 0;
        do
        {
            Map<String, Integer>  DadosOpcao = IO.solicitarOpcaoMenu();
            opcaoMenu = DadosOpcao.get("opcao");

            if(opcaoMenu == 1)
                exibirOpcaoImprimirContato(agenda);
            else if (opcaoMenu == 2)
                exibirOpcaoRemoverContato (agenda);
            else if (opcaoMenu == 3)
                exibirOpcaoAdicionarContato(agenda);
            else if(opcaoMenu == 4)
                agenda.imprimir();
        }
        while(opcaoMenu != 5);
    }

    private static void exibirOpcaoImprimirContato(Agenda agenda)
    {
        try
        {
            Map<String, String> dadosBuscaBusca = IO.solicitarTextoParaBuscaImpressao();
            agenda.imprimirPessoa(dadosBuscaBusca.get("texto"));
        }
        catch (ArithmeticException ex)
        {
            IO.imprimirException(ex);
        }
    }

    private static void exibirOpcaoRemoverContato(Agenda agenda)
    {
        try
        {
            Map<String, String> dadosBuscaExclusao = IO.solicitarTextoParaBuscaRemover();
            agenda.removerPessoaPorEmail(dadosBuscaExclusao.get("texto"));
            IO.exibirMensagemExclusao();
        }
        catch (ArithmeticException ex)
        {
            IO.imprimirException(ex);
        }
    }

    private static void exibirOpcaoAdicionarContato(Agenda agenda)
    {
        try
        {
            Map<String, String> dadosContato = IO.solicitarDadosContato();
            agenda.cadastrar(dadosContato.get("nome"), dadosContato.get("telefone"), dadosContato.get("email"));
            IO.exibirMensagemInclusao();
        }
        catch (ArithmeticException ex)
        {
            IO.imprimirException(ex);
        }
    }
}
