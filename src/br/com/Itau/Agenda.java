package br.com.Itau;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Agenda implements Impressao
{
    List<Contato> Contatos;

    public Agenda(int QtdPessoas)
    {
        Contatos = new ArrayList<>();

        for(int i=0;i<QtdPessoas;i++)
        {
            Map<String, String>  dadosContato = IO.solicitarDadosContato();
            cadastrar(dadosContato.get("nome"), dadosContato.get("telefone"), dadosContato.get("email"));
        }
    }

    public void cadastrar(String nome, String telefone, String email)
    {
        Contato contato = new Contato(nome, telefone, email);
        Contatos.add(contato);
    }

    private Contato buscarPorEmail(String email)
    {
        Contato contatoBuscado = null;
        for (Contato contato:Contatos)
        {
            if(contato.getEmail().equals(email))
                contatoBuscado = contato;
        }

        return contatoBuscado;
    }

    private Contato buscarPorNumero(String numero)
    {
        Contato contatoBuscado = null;
        for (Contato contato:Contatos)
        {
            if(contato.getTelefone().equals(numero))
                contatoBuscado = contato;
        }

        return contatoBuscado;
    }

    public void removerPessoaPorEmail(String email)
    {
        Contato contato = buscarPorEmail(email);

        if(contato != null)
            Contatos.remove(contato);
        else
            throw new ArithmeticException("Contato não encontrado");
    }

    public void imprimirPessoa(String texto)
    {
        Contato contato = buscarPorNumero(texto);
        if(contato == null)
            contato = buscarPorEmail(texto);

        if(contato != null)
            contato.imprimir();
        else
            throw new ArithmeticException("Contato não encontrado");
    }

    @Override
    public void imprimir()
    {
        for (Contato contato:Contatos)
        {
            contato.imprimir();
        }
    }
}
